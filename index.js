var fs = require('fs')
  , gm = require('gm').subClass({imageMagick: true});

  const origin = 'png'
  const res = 'svg'
	var arr = [];

	fs.readdirSync('./test').forEach(file => {
		if (file.indexOf(`.${origin}`) !== -1) {
			arr.push(file.slice(0, -4));
		}
	})

	console.log(arr);

	arr.forEach((item) => {
		compressImage(item);
		// svgToPng(item);
	})

function svgToPng(imageName) {
  gm(`test/${imageName}.${origin}`)
  .write(`res/${imageName}.png`, function (err) {
    if (err){
      console.log('ERR', err);
    } else {
      console.log('done');
    }
  });
}

function compressImage(imageName) {
	const size = '100';
	gm(`test/${imageName}.${origin}`)
	.quality(30)
	.resize(size, size, '^')
  .gravity('Center')
	.write(`res/${imageName}.${res}`, function (err) {
	  if (err){
			console.log('ERR', err);
		} else {
			console.log('done');
		}
	});
}
